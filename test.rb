# require section
require 'bcrypt'
require 'benchmark'

# config section
BCrypt::Engine.cost = 12

cipher = OpenSSL::Cipher::AES.new(256, :CBC)
cipher.encrypt
key = cipher.random_key
iv = cipher.random_iv

#init section
alphabet = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
random_string = ''
encrypted = ''

16.times do
	random_string += alphabet.sample
end

hash = BCrypt::Password.create("my password")
data = 'accept;' + hash + ';' + random_string + ';' + Time.now.to_i.to_s

# tested section
p Benchmark.measure { encrypted += cipher.update(data) + cipher.final }

p encrypted
